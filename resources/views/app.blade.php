<!DOCTYPE html>
<html>

<head>
    <title>News & Bloging</title>
    <link type="text/css" rel="stylesheet" href="{{ asset('/css/plugins.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('/css/application.css') }}">

    @yield("header")

</head>

<body class="@yield('modedefault')">
    <div id="fb-root"></div>
    @include("_particles.header")

    @yield("content")

    @include("_particles.footer")

    <script src="{{ asset('js/plugins.js') }}"></script>
    <script src="{{ asset('js/app.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            App.init();
        });

    </script>
    @if(getenvcong('facebookapp')>"")
    <script>
        $(function () {

            //facebook
            window.fbAsyncInit = function () {
                FB.init({
                    appId: '{{ getenvcong('
                    facebookapp ') }}',
                    xfbml: true,
                    version: 'v2.5'
                });
            };

            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/{{  getenvcong('sitelanguage') > "
                " ? getenvcong('sitelanguage') : 'en_US' }}/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

        });

    </script>
    @endif
    @yield("footer")
    @include('.errors.swalerror')

    <div id="auth-modal" class="modal auth-modal"></div>

    <div class="hide">
        <input name="_requesttoken" id="requesttoken" type="hidden" value="{{ csrf_token() }}" />
    </div>

    {!! rawurldecode(getenvcong('footercode')) !!}

</body>

</html>
