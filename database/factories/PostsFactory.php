<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Posts;
use Faker\Generator as Faker;

$factory->define(Posts::class, function (Faker $faker) {
    return [
        'user_id' => '1',
        'category_id' => '1',
        'type' => 'news',
        'slug' => str_slug($faker->sentence, '-'),
        'title' => $faker->sentence,
        'body' => $faker->realText($maxNbChars = 1200, $indexSize = 2),
        'thumb' => $faker->imageUrl($width = 355, $height = 236, $category = null, $randomize = true),
        'published_at' => $faker->date($format = 'Y-m-d H:i:s', $max = 'now'),
        'approve' => 'yes',
    ];
});
