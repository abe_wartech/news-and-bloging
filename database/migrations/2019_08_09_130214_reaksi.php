<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Reaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reactions_icons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ord');
            $table->string('icon');
            $table->string('name');
            $table->string('reaction_type');
            $table->string('display', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reactions_icons');
    }
}
