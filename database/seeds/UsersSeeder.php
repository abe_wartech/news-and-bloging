<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'usertype' => 'Admin',
            'username' => 'admin',
            'username_slug' => 'admin',
            'icon' => null,
            'email' => 'admin@database.com',
            'password' => bcrypt('12345'),
            'remember_token' => str_random(10),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
